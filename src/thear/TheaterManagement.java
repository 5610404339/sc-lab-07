package thear;

import java.util.ArrayList;

public class TheaterManagement {
	 double ticketPrices[][]; 
	 private DataSeatPrice s;
	 private ShowTime sq;
	 private ArrayList<Double> total  = new ArrayList<Double>();
	 
	 public TheaterManagement (){
		 s = new DataSeatPrice();
	 } 
		public void setTime(String strday, String strR) {
			sq.setTime(strday,strR);
		}
	 	public void addSeatsPrice(double amount){
	 		total.add(amount);
	 	}
	 	public double getSeatsPrice(){
	 		double ans =0;
	 		for(int x=0 ;x<total.size();x++){
	 			ans+=total.get(x);
	 		}
	 		return ans;
	 	}
	 	
	 	public void clear(){
	 		total.clear();
	 	}

		 public double getPrice(int i, int j){
			 return s.getPrice(i, j);
		 }
		 public void setSeat(int i, int j){
			s.setSeat(i, j);
		 }
		 public double[][] getSeat(){
			 return s.getTicketSeat();
		 }
}
